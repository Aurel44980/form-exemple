import React, {useState, useEffect} from 'react'
import logo from './logo.svg';
import './App.css';

const App = () => {
  
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [age, setAge] = useState("")
  
  const sendForm = (e)=>{
    e.preventDefault()
    let infos = {
      firstname: firstName,
      lastname: lastName,
      age: age
    }
    console.log(infos)
    //j'ai regroupé mon objet dans lequel les propriétés deviendront des req.body lors de la réception de l'API back
    
    /*axios.post("/monurl", infos, {headers: {'x-access-token': monToken}})
    .then((res)=>{
      // ok
    })
    .catch(err=>console.log(err))*/
  }
  
  return (
    <div className="App">
      <header className="App-header">
        <h1>Mon super Formulaire</h1>
      </header>
      <main>
        <form
          onSubmit={sendForm}
        >
          <input
            type="text"
            name="prenom"
            placeholder="prénom"
            onChange={(e)=>{
             setFirstName(e.currentTarget.value) 
            }}
          />
          <input
            type="text"
            name="nom"
            placeholder="nom"
            onChange={(e)=>{
             setLastName(e.currentTarget.value) 
            }}
          />
          <input
            type="text"
            name="age"
            placeholder="age"
            onChange={(e)=>{
             setAge(e.currentTarget.value) 
            }}
          />
          <button>Envoyer</button>
        </form>
      </main>
    </div>
  );
}

export default App;

